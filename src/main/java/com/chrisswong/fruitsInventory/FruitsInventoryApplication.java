package com.chrisswong.fruitsInventory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FruitsInventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(FruitsInventoryApplication.class, args);
	}

}
