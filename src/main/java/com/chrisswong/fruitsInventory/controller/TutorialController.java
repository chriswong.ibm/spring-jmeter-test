package com.chrisswong.fruitsInventory.controller;

import com.chrisswong.fruitsInventory.model.Tutorial;
import com.chrisswong.fruitsInventory.repository.TutorialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class TutorialController {

    @Autowired
    TutorialRepository repo;

    @GetMapping("/tutorials")
    public ResponseEntity<List<Tutorial>> getAllTutorial() {
        List<Tutorial> tutorials = repo.findAll();

        if (tutorials.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(tutorials, HttpStatus.OK);
        }
    }

    @PostMapping("/tutorials")
    public ResponseEntity<Tutorial> createTutorial(@RequestBody Tutorial tutorial) {
        try {
            Tutorial toBeSavedTut  = new Tutorial();
            toBeSavedTut.setTitle(tutorial.getTitle());
            Tutorial _tut = repo.save(toBeSavedTut);
            return new ResponseEntity<>(_tut, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/tutorials")
    public ResponseEntity<Tutorial> updateTutorial(@RequestBody Tutorial tutorial) {
        try {
            Optional<Tutorial> toBeUpdatedTut = repo.findById(tutorial.getId());
            if(toBeUpdatedTut.isPresent()) {
                Tutorial foundTut = toBeUpdatedTut.get();
                foundTut.setTitle(tutorial.getTitle());
                Tutorial _tut = repo.save(foundTut);
                return new ResponseEntity<>(_tut, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/tutorials/{id}")
    public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") long id) {
        try {
            repo.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
