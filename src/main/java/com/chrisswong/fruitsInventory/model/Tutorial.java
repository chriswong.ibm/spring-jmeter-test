package com.chrisswong.fruitsInventory.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "tutorials")
@Data
public class Tutorial {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;
}
